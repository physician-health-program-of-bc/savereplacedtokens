# savereplacedtokens

When sending a non-bulk email, e.g. using Contacts - New Email, what gets stored on the resulting activity for tokens are the literal tokens themselves, which can be useful for troubleshooting but you also often want to see the replaced values.

The approach here is different than the approach in https://lab.civicrm.org/dev/core/-/issues/1750. In that approach, there are separate activities recorded for each recipient. Here, there's just one activity, and both the original tokens and per-recipient replacements are visible on the activity. The extension maintains a separate database table to record the replaced values.

Currently a small core hack is required to make the activity and contact ids available to the alterMailParams hook.

The extension is licensed under [MIT](LICENSE.txt).

![Screenshot](/images/screenshot.png)

## Requirements

* PHP 5.3+
* CiviCRM 5+

## Installation

Clone the [Git](https://en.wikipedia.org/wiki/Git) repo for this extension and then either install it with the command-line tool [cv](https://github.com/civicrm/cv) or from the Web UI.

```bash
git clone https://lab.civicrm.org/extensions/savereplacedtokens.git
cv en savereplacedtokens (or install from Administer - System Settings - Extensions).
```

** **THIS EXTENSION REQUIRES A SMALL PATCH TO CORE** **

In file CRM/Activity/BAO/Activity.php, in the sendMessage() function, where it creates the mailParams array, add two lines on the end, i.e. change

```php
    $mailParams = [
      'groupName' => 'Activity Email Sender',
      'from' => $from,
      'toName' => $toDisplayName,
      'toEmail' => $toEmail,
      'subject' => $subject,
      'cc' => $cc,
      'bcc' => $bcc,
      'text' => $text_message,
      'html' => $html_message,
      'attachments' => $attachments,
    ];
```

to

```php
    $mailParams = [
      'groupName' => 'Activity Email Sender',
      'from' => $from,
      'toName' => $toDisplayName,
      'toEmail' => $toEmail,
      'subject' => $subject,
      'cc' => $cc,
      'bcc' => $bcc,
      'text' => $text_message,
      'html' => $html_message,
      'attachments' => $attachments,
      'activityID' => $activityID,    // <-- these two lines
      'toID' => $toID,                // <-- these two lines
    ];
```

## Known Issues

1. Requires the core hack mentioned above.
2. Having all the data in one activity may or may not be compatible with GDPR or similar privacy requirements. See the lab ticket mentioned above.
3. Case merges will cause havoc, as they do for everything. But if you can get to Manage Case for the deleted case you'd still be able to view the data on the deleted activity there.
4. I haven't tested every possible variation of popups and not popups and the multiple variations of `/civicrm/activity/xx` urls. It's possible some variation might not display the extra accordions. Please file an issue.
