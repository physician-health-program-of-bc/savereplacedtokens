<?php
/* vim: set shiftwidth=2 tabstop=2 softtabstop=2: */

/**
 * Collection of upgrade steps.
 */
class CRM_Savereplacedtokens_Upgrader extends CRM_Savereplacedtokens_Upgrader_Base {

  // By convention, functions that look like "function upgrade_NNNN()" are
  // upgrade tasks. They are executed in order (like Drupal's hook_update_N).

  /**
   * Create our database table
   */
  public function install() {
    // older versions don't have CRM_Core_BAO_SchemaHandler::getInUseCollation()
    $dao = CRM_Core_DAO::executeQuery("SHOW TABLE STATUS LIKE 'civicrm_contact'");
    $dao->fetch();
    $charset = 'utf8';
    if (stripos($dao->Collation, 'utf8mb4') !== FALSE) {
      $charset = 'utf8mb4';
    }

    // With token replacements, `subject` might end up longer than 255.
    // To think about...

    CRM_Core_DAO::executeQuery("CREATE TABLE IF NOT EXISTS `savereplacedtokens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Default MySQL primary key',
  `activity_id` int(10) unsigned NOT NULL COMMENT 'FK to civicrm_activity',
  `contact_id` int(10) unsigned NOT NULL COMMENT 'FK to civicrm_contact',
  `subject` varchar(255) COLLATE {$dao->Collation} DEFAULT NULL COMMENT 'email subject',
  `text` longtext COLLATE {$dao->Collation} DEFAULT NULL COMMENT 'email body text version',
  `html` longtext COLLATE {$dao->Collation} DEFAULT NULL COMMENT 'email body html version',
  PRIMARY KEY (`id`),
  KEY `FK_savereplacedtokens_activityid` (`activity_id`),
  KEY `FK_savereplacedtokens_contactid` (`contact_id`),
  CONSTRAINT `FK_savereplacedtokens_activityid` FOREIGN KEY (`activity_id`) REFERENCES `civicrm_activity` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_savereplacedtokens_contactid` FOREIGN KEY (`contact_id`) REFERENCES `civicrm_contact` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET={$charset} COLLATE={$dao->Collation}");

    $dao->free();

    // Since our table doesn't start with `civicrm_`, we don't need to think
    // about log tables or schema reconciliation.
  }

  /**
   * Delete our table on uninstall.
   */
  public function uninstall() {
    CRM_Core_DAO::executeQuery("DROP TABLE IF EXISTS savereplacedtokens");
  }

}
