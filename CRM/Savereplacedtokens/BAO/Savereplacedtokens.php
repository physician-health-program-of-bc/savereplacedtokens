<?php
/* vim: set shiftwidth=2 tabstop=2 softtabstop=2: */

/**
 * Backend functions.
 */
class CRM_Savereplacedtokens_BAO_Savereplacedtokens {

  /**
   * Store the email data after the tokens have been replaced.
   * @param array $params
   *   The array from hook_civicrm_alterMailParams which originally comes from
   *   CRM_Activity_BAO_Activity::sendMessage().
   */
  public static function saveReplacedTokens($params) {
    if (empty($params['activityID']) || empty($params['toID'])) {
      return;
    }

    $sql_params = array(
      1 => array($params['activityID'], 'Integer'),
      2 => array($params['toID'], 'Integer'),
      3 => array(empty($params['subject']) ? '' : $params['subject'], 'String'),
      4 => array(empty($params['text']) ? '' : $params['text'], 'String'),
      5 => array(empty($params['html']) ? '' : $params['html'], 'String'),
    );
    CRM_Core_DAO::executeQuery("INSERT INTO savereplacedtokens (activity_id, contact_id, subject, text, html) VALUES (%1, %2, %3, %4, %5)", $sql_params);
  }

  /**
   * Given an activity id, find the actual activity id where we recorded the
   * data, which might be an earlier (civicase) revision.
   *
   * We can assume it will be the original revision because that's when the
   * email would have been sent.
   *
   * Merges will f- this up, as they do for everything.
   *
   * @param int $id
   * @return array
   *   The email data we recorded.
   */
  public static function getData($id) {
    $ret = array();
    $params = array(1 => array($id, 'Integer'));
    $dao = CRM_Core_DAO::executeQuery("SELECT r.*, c.sort_name
FROM savereplacedtokens r
LEFT JOIN civicrm_contact c ON c.id = r.contact_id
WHERE r.activity_id = %1
OR r.activity_id IN (
  SELECT a.original_id FROM civicrm_activity a WHERE a.id = %1
)", $params);
    while ($dao->fetch()) {
      $ret[$dao->contact_id] = array(
        'activity_id' => $dao->activity_id,
        'contact_id' => $dao->contact_id,
        'sort_name' => $dao->sort_name,
        'subject' => $dao->subject,
        'text' => $dao->text,
        'html' => $dao->html,
      );
    }
    $dao->free();
    return $ret;
  }

}
