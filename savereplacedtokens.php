<?php
/* vim: set shiftwidth=2 tabstop=2 softtabstop=2: */

require_once 'savereplacedtokens.civix.php';

/**
 * Implementation of hook_civicrm_config
 */
function savereplacedtokens_civicrm_config(&$config) {
  _savereplacedtokens_civix_civicrm_config($config);
}

/**
 * Implementation of hook_civicrm_xmlMenu
 *
 * @param $files array(string)
 */
function savereplacedtokens_civicrm_xmlMenu(&$files) {
  _savereplacedtokens_civix_civicrm_xmlMenu($files);
}

/**
 * Implementation of hook_civicrm_install
 */
function savereplacedtokens_civicrm_install() {
  return _savereplacedtokens_civix_civicrm_install();
}

/**
 * Implementation of hook_civicrm_uninstall
 */
function savereplacedtokens_civicrm_uninstall() {
  return _savereplacedtokens_civix_civicrm_uninstall();
}

/**
 * Implementation of hook_civicrm_enable
 */
function savereplacedtokens_civicrm_enable() {
  return _savereplacedtokens_civix_civicrm_enable();
}

/**
 * Implementation of hook_civicrm_disable
 */
function savereplacedtokens_civicrm_disable() {
  return _savereplacedtokens_civix_civicrm_disable();
}

/**
 * Implementation of hook_civicrm_upgrade
 *
 * @param $op string, the type of operation being performed; 'check' or 'enqueue'
 * @param $queue CRM_Queue_Queue, (for 'enqueue') the modifiable list of pending up upgrade tasks
 *
 * @return mixed  based on op. for 'check', returns array(boolean) (TRUE if upgrades are pending)
 *                for 'enqueue', returns void
 */
function savereplacedtokens_civicrm_upgrade($op, CRM_Queue_Queue $queue = NULL) {
  return _savereplacedtokens_civix_civicrm_upgrade($op, $queue);
}

/**
 * Implementation of hook_civicrm_managed
 *
 * Generate a list of entities to create/deactivate/delete when this module
 * is installed, disabled, uninstalled.
 */
function savereplacedtokens_civicrm_managed(&$entities) {
  return _savereplacedtokens_civix_civicrm_managed($entities);
}

/**
 * Implementation of hook_civicrm_alterMailParams
 */
function savereplacedtokens_civicrm_alterMailParams(&$params, $context = NULL) {
  // $context isn't available in earlier versions, so be friendly
  if (!is_null($context)) {
    if ($context !== 'singleEmail') {
      return;
    }
  }

  CRM_Savereplacedtokens_BAO_Savereplacedtokens::saveReplacedTokens($params);
}

/**
 * Implementation of hook_civicrm_buildForm
 */
function savereplacedtokens_civicrm_buildForm($formName, $form) {
  $aid = NULL;
  $action = NULL;
  switch ($formName) {
    case 'CRM_Activity_Form_ActivityView':
      $aid = CRM_Utils_Request::retrieve('id', 'Integer');
      $action = CRM_Utils_Request::retrieve('action', 'String');
      break;
    case 'CRM_Case_Form_ActivityView':
      $aid = CRM_Utils_Request::retrieve('aid', 'Integer');
      $action = CRM_Core_Action::VIEW;
      break;
  }

  if (empty($action) || $action != CRM_Core_Action::VIEW || empty($aid)) {
    return;
  }

  $data = CRM_Savereplacedtokens_BAO_Savereplacedtokens::getData($aid);
  if (!empty($data)) {
    $form->assign('savereplacedtokens', $data);
    CRM_Core_Region::instance('page-footer')->add(array(
      'template' => 'CRM/Savereplacedtokens/EmailDetails.tpl',
    ));
  }
}
