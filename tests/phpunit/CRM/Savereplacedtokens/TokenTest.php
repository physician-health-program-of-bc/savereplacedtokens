<?php

/**
 * Tests for Token Replacement
 *
 * @group headless
 */
class CRM_Savereplacedtokens_TokenTest extends CiviUnitTestCase {

  public function setUpHeadless() {
    return \Civi\Test::headless()
      ->installMe(__DIR__)
      ->apply();
  }

  /**
   * Check that we haven't forgotten to apply the needed hack to core.
   */
  public function testHackIsInPlace() {
    $subject = 'test subject';
    $plain = 'test plain';
    $html = '<p>test html</p>';
    // Dummy activity just needs to be present
    $activity = $this->callAPISuccess('Activity', 'create', [
      'activity_type_id' => 'Email',
      'source_contact_id' => 1,
      'subject' => $subject,
    ]);
    // set up hook
    \Civi::dispatcher()->addListener('hook_civicrm_alterMailParams', [$this, 'hookForAlterMailParams']);
    // The hook should crash the test if the patch isn't applied.
    CRM_Activity_BAO_Activity::sendMessage(
      'abc@def.ghi',
      1,
      1,
      $subject,
      $plain,
      $html,
      'abc@def.ghi',
      $activity['id'],
    );
  }

  /**
   * This is the listener for hook_civicrm_alterMailParams
   *
   * @param \Civi\Core\Event\GenericHookEvent $e
   */
  public function hookForAlterMailParams(\Civi\Core\Event\GenericHookEvent $e) {
    // In order to get the helpful message displayed we have to prevent the
    // access of the element from itself throwing an error, so that's why this
    // assert seems awkward.
    $this->assertFalse(empty($e->params['activityID']), 'Patch to core has probably not been applied?');
    $this->assertFalse(empty($e->params['toID']), 'Patch to core has probably not been applied?');
  }

}
