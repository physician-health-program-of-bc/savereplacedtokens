{if !empty($savereplacedtokens)}
  <table id="savereplacedtokens-details">
  {foreach from=$savereplacedtokens item=email}
    <tr><td>
      <div class="crm-accordion-wrapper collapsed">
        <div class="crm-accordion-header">
          {ts 1=$email.sort_name|escape}Message Details for <em>%1</em>{/ts}
        </div><!-- /.crm-accordion-header -->
        <div class="crm-accordion-body">
          <div id="messagedetail-{$email.contact_id}" class="view-content">
            <div class="activity-subject">{ts 1=$email.subject|escape}Subject: %1{/ts}</div>
          {if empty($email.html)}
            {$email.text|escape}
          {else}
            {$email.html|purify}
          {/if}
          </div>
        </div><!-- /.crm-accordion-body -->
      </div><!-- /.crm-accordion-wrapper -->
    </td></tr>
  {/foreach}
  </table>
{/if}
{literal}
<script type="text/javascript">
cj(function() {
  if (cj('div.crm-case-activity-view-block table.crm-info-panel').length) {
    cj('#savereplacedtokens-details').insertAfter('div.crm-case-activity-view-block table.crm-info-panel');
  } else {
    cj('#savereplacedtokens-details').insertAfter('div.crm-activity-view-block table.crm-info-panel');
  }
});
</script>
{/literal}
